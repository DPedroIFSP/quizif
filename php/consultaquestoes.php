<?php
ini_set("display_errors", 1);
ini_set('display_startup_errors', 1);
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
error_reporting(E_ALL);


// echo '<pre>' . var_export($_GET) . '</pre>';

$hostname = "localhost";
$user = "root";
$password = "ifsp";
$database = "quizif";

$conn = mysqli_connect($hostname, $user, $password, $database);

if (!$conn) {
    die("Conexão falhou: " . mysqli_connect_error());
}

$query = "select * from questoes;";
$results = mysqli_query($conn, $query);
$index = 0;
while ($record = mysqli_fetch_row($results)) {
    $question = array(
        'id' => $record[0],
        'titulo' => $record[1],
        'descricao' => $record[2],
        'Materia'=> $record[3],
        'nivel' => $record[4],
        'op a' => $record[5],
        'op b' => $record[6],
        'op c' => $record[7],
        'op d' => $record[8],
        'op e' => $record[9],
        'op correta' => $record[10]

    );
    $questions[$index] = $question;
    $index++;
}


mysqli_close($conn);
//$formattedData =  json_encode($questions, JSON_PRETTY_PRINT);
echo json_encode($questions);

// header("Location: /index.html");
// exit();

?>
